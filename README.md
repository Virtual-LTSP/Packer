## Introduction

Packer is an open source tool for creating identical machine images from a single source configuration.
you can read more about it here - https://www.packer.io

## Installation

```bash
curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
sudo apt-get update && sudo apt-get install packer
```

## Validate packer template

```bash
packer validate linuxmint-20.2-xfce.json
```

## Build linux mint vagrant box

```
packer build linuxmint-20.2-xfce.json
```
